Boing ball screen saver for OS X, version 0.1

Made by David Revelj 2008

Download: [boing_qtz.zip](https://bitbucket.org/revelj/quartz-boing/downloads/boing_qtz.zip)

![boing.png](https://bitbucket.org/repo/9ea5rn/images/2553677040-boing.png)